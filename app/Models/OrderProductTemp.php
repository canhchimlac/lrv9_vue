<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProductTemp extends Model
{
    protected $table = "tbl_order_product_temp";
    protected $guarded = [];
};
