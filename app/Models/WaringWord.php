<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WaringWord extends Model
{
    protected $table = "tblwaringwords";
    protected $guarded = [];
};