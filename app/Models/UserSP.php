<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSP extends Model
{
    protected $table = "tbl_user_sp";
    protected $guarded = [];
};