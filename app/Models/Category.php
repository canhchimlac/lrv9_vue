<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "tbl_product_categories";
    protected $guarded = [];
};

