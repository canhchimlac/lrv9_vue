<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderTemp extends Model
{
    protected $table = "tbl_order_temp";
    protected $guarded = [];

};
