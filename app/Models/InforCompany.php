<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InforCompany extends Model
{
    protected $table = "tbl_company_infor";
    protected $guarded = [];
}
